
#### Ejercicio 6 ###

from mysoundsin import SoundSin
import unittest

class TestSound(unittest.TestCase):
    def test_values(self):
        sound = SoundSin(2, 440, 10000)


        # Check that sin wave values are within expected range
        self.assertTrue(all(-SoundSin.max_amplitude <= x <= SoundSin.max_amplitude
                            for x in sound.buffer))
        # Check specific sample values which should be 0
        self.assertEqual(0, sound.buffer[0])
        self.assertEqual(0, sound.buffer[22050])
        self.assertEqual(0, sound.buffer[44100])

    def test_nsamples(self):
        sound = SoundSin(2, 440, 10000)
    # Check specific sample values which should be maximu
    # Maximum amplitude in Pi/4 samples (i + 0.25 periods)
        period = 44100 / 440
        self.assertAlmostEqual(10000, sound.buffer[int(period * 0.25)],delta=2)


class TestBars(unittest.TestCase):

    def test_bars(self):
        sound = SoundSin(0.01,440,10000)


        bars = sound.bars(bar_period=0.0001)

        # Check number of lines
        self.assertAlmostEqual(112, len(bars.split('\n')), delta=1)

        # Check bar length
        self.assertAlmostEqual(41, len(bars.split('\n')[0]), delta=1)

        # Check positive bar
        self.assertEqual(' ' * 40 + ':', bars.split('\n')[0])
        self.assertEqual(' ' * 40 + ':' + '*' * 12, bars.split('\n')[1])
        self.assertEqual(' ' * 40 + ':' + '*' * 20, bars.split('\n')[2])
        self.assertEqual(' ' * 40 + ':' + '*' * 28, bars.split('\n')[3])
        self.assertEqual(' ' * 40 + ':' + '*' * 32, bars.split('\n')[4])
        self.assertEqual(' ' * 40 + ':' + '*' * 36, bars.split('\n')[5])

        # Check negative bar
        self.assertEqual(' ' * 33, bars.split('\n')[9][:33])



if __name__ == '__main__':
    unittest.main()