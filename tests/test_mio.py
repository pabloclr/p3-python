

### Ejercicio 4 ###


from mysound import Sound
import unittest
import math

class Testmio(unittest.TestCase):
    def test_sin(self):
        # Create a sound with a sine wave
        s = Sound(1.0)
        s.sin(440, 0.5)

        # Ensure the buffer has the correct length
        self.assertEqual(len(s.buffer), s.nsamples)

        # Ensure the values in the buffer correspond to a sine wave
        for nsample in range(s.nsamples):
            t = nsample / s.samples_second
            expected_value = int(0.5 * math.sin(2 * math.pi * 440 * t))
            self.assertEqual(s.buffer[nsample], expected_value)



if __name__ == '__main__':
    unittest.main()


