import unittest

#### Ejercicio 8 ###

from soundops import soundadd
from mysound import Sound
import random

class TestSoundAdd(unittest.TestCase):

    def test_same_sound(self):
        s1 = Sound(1.0)
        s1.buffer= [1,2,3]
        s2 = Sound(1.0)
        s2.buffer= [1,2,3]
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 1.0)

    def test_different_sound(self):
        s1 = Sound(1.0)
        s1.buffer = [10,20,30]
        s2 = Sound(1.0)
        s2.buffer = [20,30,40,50]
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, 1.0)

    def test_empty_sound(self):
        num1 = random.randint(1, 5)
        s1 = Sound(0)
        s2 = Sound(num1)
        result = soundadd(s1, s2)
        self.assertEqual(result.duration, num1)


if __name__ == '__main__':
    unittest.main()
